import Vue from 'vue'
import Vuex from 'vuex'
import templates from './modules/templates'

Vue.use(Vuex)

export default new Vuex.Store({
  namespaced: true,
  strict: true,
  modules: {
    templates,
  }
})