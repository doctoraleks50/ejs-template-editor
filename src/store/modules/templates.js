const axios = require('axios');
const host = "https://gitlab.com";
const gitlab_project_id = 20913585;
const private_token = "glpat-MM_CJ_-zHgkz7Zhjyi39";

export default {
  namespaced: true,
  state: {
    templateList: [],
    templateJson: '',
    templateEjs: '',
    originTemplateJson: '',
    originTemplateEjs: '',
  },
  actions: {
    async fetchTemplateList({commit}) {
      const {data} = await axios.get(`${host}/api/v4/projects/${gitlab_project_id}/repository/tree?ref=master&path=templates`, {
        headers: {
          'PRIVATE-TOKEN': private_token
        }
      });
      const templateList = data.map(template => template.name.split('.')[0]);
      commit('setTemplateList', templateList)
    },
    async fetchTemplate({dispatch}, name) {
      await dispatch('fetchTemplateJson', name);
      await dispatch('fetchTemplateEjs', name);
    },
    async fetchTemplateJson({commit}, name) {
      const {data: templateJson} = await axios.get(`${host}/api/v4/projects/${gitlab_project_id}/repository/files/json_data%2F${name}.json/raw?ref=master`, {
        headers: {
          'PRIVATE-TOKEN': private_token
        }
      });

      commit('setTemplateJson', templateJson)
      commit('setOriginTemplateJson', templateJson)
    },
    async fetchTemplateEjs({commit}, name) {
      const {data: templateEjs} = await axios.get(`${host}/api/v4/projects/${gitlab_project_id}/repository/files/templates%2F${name}.ejs/raw?ref=master`, {
        headers: {
          'PRIVATE-TOKEN': private_token
        }
      })
      commit('setTemplateEjs', templateEjs);
      commit('setOriginTemplateEjs', templateEjs);
    },
    async updateTemplate({state, commit, dispatch}, {name, isEjsDataChanged, isJsonDataChanged}) {
      if (isJsonDataChanged) {
        await axios.put(`${host}/api/v4/projects/${gitlab_project_id}/repository/files/json_data%2F${name}.json?private_token=${private_token}`, {
          branch: 'master',
          content: JSON.stringify(state.templateJson),
          commit_message: `${new Date()}`
        });
      }
      if (isEjsDataChanged) {
        await axios.put(`${host}/api/v4/projects/${gitlab_project_id}/repository/files/templates%2F${name}.ejs?private_token=${private_token}`, {
          branch: 'master',
          content: state.templateEjs,
          commit_message: `${new Date()}`
        });
      }
      if (isJsonDataChanged || isEjsDataChanged) {
        await dispatch('fetchTemplate', name);
      }
    },
    async createTemplate({state, commit, dispatch}, {name}) {
      await axios.post(`${host}/api/v4/projects/${gitlab_project_id}/repository/files/json_data%2F${name}.json?private_token=${private_token}`, {
        branch: 'master',
        content: JSON.stringify(state.templateJson),
        commit_message: `${new Date()}`
      });
      await axios.post(`${host}/api/v4/projects/${gitlab_project_id}/repository/files/templates%2F${name}.ejs?private_token=${private_token}`, {
        branch: 'master',
        content: state.templateEjs,
        commit_message: `${new Date()}`
      });
      await dispatch('fetchTemplateList', name);
    },
    async deleteTemplate({state, commit, dispatch}, {name}) {
      console.log({name})
      await axios.delete(`${host}/api/v4/projects/${gitlab_project_id}/repository/files/json_data%2F${name}.json?private_token=${private_token}&branch=master&commit_message=${new Date()}`);
      await axios.delete(`${host}/api/v4/projects/${gitlab_project_id}/repository/files/templates%2F${name}.ejs?private_token=${private_token}&branch=master&commit_message=${new Date()}`);
      await dispatch('fetchTemplateList', name);
    },
  },
  mutations: {
    setTemplateList(state, templateList) {
      state.templateList = templateList
    },
    setTemplateJson(state, templateJson) {
      state.templateJson = templateJson;
    },
    setTemplateEjs(state, templateEjs) {
      state.templateEjs = templateEjs;
    },
    setOriginTemplateJson(state, originTemplateJson) {
      state.originTemplateJson = originTemplateJson;
    },
    setOriginTemplateEjs(state, originTemplateEjs) {
      state.originTemplateEjs = originTemplateEjs;
    },
  },
};
